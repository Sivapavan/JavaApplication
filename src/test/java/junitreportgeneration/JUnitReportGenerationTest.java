package junitreportgeneration;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import com.mkyong.common.controller.JSONController;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class JUnitReportGenerationTest {

	@Test
	public void Addition() {
		JSONController controller = new JSONController();
		
		String data = "2+4";
		String val = controller.getShopInJSON(data);
		assertThat(val, is("6.0"));
	}

	
	@Test
	public void Subtract() {
		JSONController controller = new JSONController();
		String data = "4-2";
		String val = controller.getShopInJSON(data);
		assertThat(val, is("2.0"));
	}

	
	@Test
	public void Multiply() {
		JSONController controller = new JSONController();
		String data = "2*4";
		String val = controller.getShopInJSON(data);
		assertThat(val, is("8.0"));
	}

	
	@Test
	public void Division() {
		JSONController controller = new JSONController();
		String data = "8/4";
		String val = controller.getShopInJSON(data);
		assertThat(val, is("2.0"));
	}
}
