package com.mkyong.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.net.URLDecoder;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/calc")
public class JSONController {

	 
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String getShopInJSON(@RequestParam("name") String name) {

		String data = name;
		if (data != null) {
			if (data.charAt(data.length() - 1) == '+' || data.charAt(data.length() - 1) == '-'
					|| data.charAt(data.length() - 1) == '*' || data.charAt(data.length() - 1) == '/') {
				return data;
			} else {
				data = GetResult(data);
				return data;

			}
		}

		return data;

	}

	public String GetResult(String str) {
		List<Character> symbleList = new ArrayList<Character>();
		// char[] charSymble = { '+', '-', '*', '/' };
		String[] st = str.split("[+ * \\- /]");
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == '+' || str.charAt(i) == '-' || str.charAt(i) == '*' || str.charAt(i) == '/') {
				symbleList.add(new Character(str.charAt(i)));
			}
		}
		double result = Double.parseDouble(st[0]);
		for (int i = 1; i < st.length; i++) {
			double num = Double.parseDouble(st[i]);
			int j = i - 1;
			switch (symbleList.get(j).charValue()) {
			case '+':
				result = result + num;
				break;
			case '-':
				result = result - num;
				break;
			case '*':
				result = result * num;
				break;
			case '/':
				result = result / num;
				break;
			default:
				result = 0.0;
				break;
			}
		}
		return String.valueOf(result);
	}

}